package mx.unitec.practica3

import android.app.TimePickerDialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_picker.*
import mx.unitec.practica3.ui.TimePickerFragment

class PickerActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_picker)
    }

    fun showTimePickerDialog(v : View){
        //val timerPicketFragment = TimePickerFragment()

        val timerPicketFragment = TimePickerFragment.newInstance(TimePickerDialog.OnTimeSetListener
        { view, hourOfDay, minute ->

            pkrTime.setText("${hourOfDay}:${minute}")
        })

        timerPicketFragment.show(supportFragmentManager, "timePicker")




    }
}